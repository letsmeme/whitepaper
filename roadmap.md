# ✈ Roadmap

* [x] 2022Q4
  * [x] Seed Round
  * [ ] Introduce the Chrome extension
  * [ ] Community cooperation intention collection
* [ ] 2023Q1
* [ ] 2023Q2

<figure><img src="https://2395517492-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FmOeGFztYovyUB7eZtUru%2Fuploads%2FT5yPkbkElgLIMwrhkSKI%2F%E8%9E%A2%E5%B9%95%E6%88%AA%E5%9C%96%202022-10-29%20%E4%B8%8A%E5%8D%8811.41.42.png?alt=media&#x26;token=50c088cc-0d6e-4473-9204-94d458d508e1" alt=""><figcaption><p>This is an example</p></figcaption></figure>
