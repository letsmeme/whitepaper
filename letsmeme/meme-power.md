---
description: Formula & Description
---

# MeMe Power

MeMe Power is designed to quantify the social influence of an account, which equates to the quality and quantity of traffic generated. MeMe power is calculated using the following criteria:

1. Basic Social Influence Score (S1)
2. $MEME / TCG Staking Bonus (S2)
3. Team Up Bonus (S3)

The final MeMe power is defined as the product between the basic score and the sum of the two bonuses:

$$
\text{MeMe Power} = S_1 * S_2 + S_3
$$

#### Basic Social Influence Score (S1)

The social impact of a user’s account is calculated via the following analysis. The basic social influence influence (S1) is defined as the SparkScore. The S1 is scored out of 100 and is the accumulation of the following five assessment parts:

1. Number of Followers (35%)
2. Number of Retweets (35%)
3. Number of Likes Received (10%)
4. Number of Lists On (10%)
5. Account Verified (10%)

An example of S1 Score is shown as follows:

<figure><img src="../.gitbook/assets/image (4).png" alt=""><figcaption><p>S1 Score  @elonmusk </p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (5).png" alt=""><figcaption><p>S1 Score Breakdown @elonmusk </p></figcaption></figure>

<figure><img src="../.gitbook/assets/image.png" alt=""><figcaption><p>S1 Score  @turtlecasegang</p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (1).png" alt=""><figcaption><p>S1 Score Breakdown @turtlecasegang</p></figcaption></figure>

**$MEME / TCG Staking Bonus (S2)**

If the user holds $MEME or a Turtlecase Gang (TCG) NFT, he/she would have the opportunity to lock the assets and earn an extra staking bonus. The value of S2 depends on the amount of the locked staked assets. Assuming the user locks n x TCG and m x $MEME, the S2 will be:

$$
S_2 =\text{max}(n +m /100, 1)
$$

#### Team Up Bonus (S3)

We set the team up bonus to encourage the referrals.

For each successful sign-up:

1. The user will get 2 point;
2. The invited user will get 2 point;
3. The user will get 1 point in the subsequent invitations.



