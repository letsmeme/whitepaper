---
description: service to the web3 users
---

# Customer Reward Engine

<figure><img src="../.gitbook/assets/_Trafinity - Deck 1224.png" alt=""><figcaption></figcaption></figure>

MeMe Power is defined as the social influence of an account, and the amount of $MEME distributed in the reward is adjusted accordingly.

Please refer to the MeMe Power and Tokenomics for more information.



{% content-ref url="meme-power.md" %}
[meme-power.md](meme-power.md)
{% endcontent-ref %}

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}
