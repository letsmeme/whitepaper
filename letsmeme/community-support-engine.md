---
description: service to the web3 communities
---

# Community Support Engine

## Task System

As a community administrator, you can issue tasks to all users at any time through the task system. The task types include liking, retweeting, commenting on a tweet, or following a particular account. There are two variations of task rewards: the first one being that all users who complete the task will get the reward, and the second one is a random lottery reward that will be distributed among the users who complete the task. The task system divides the tasks into two categories according to the reward points:

* Community Task

Community tasks are issued by the community administrator and are only visible to members of that community. Members can earn community points by completing the tasks

* MeMe Task

The global tasks are currently only available to the administrators of each community, and will be available to everyone in the future. Using the meme token, community advertisers can post global tasks that all let's meme users will see on their dashboard and can complete to earn meme tokens.



## Credit System

Help users view and transfer their points and other assets

* Point assets

Showing the user's points under each community. Users can transfer and redeem the points for rewards the community provides.

* Tokens
* NFT

Displays user's points under each community, the user can transfer and redeem the points for rewards that community provides.

MEME tokens, obtained by completing the MEME tasks



## Dashboard

CSE dashboard is a real-time data engine for communities to manage their members as well as get precise stats regarding the community.

​

It has the following main module:

* Chronological completion of Tasks ⌚️

Detailed statistics of community tasks completed by all members, differentiated by year, month, week and day. Tasks completed by a specific member can also be viewed by clicking on that member, allowing insight into individual members' contributions.

* Credit panel consisting of members' points[ 💯](https://emojipedia.org/hundred-points/)​

The Credit panel helps the community to quantify the contributions of each member, so that it is very clear how much they have contributed. This data can be imported, exported and modified, allowing for ease of use.

* Loyalty Insights[ ✨](https://emojipedia.org/sparkles/)​

Let's meme analyzes the user interactions on official tweets from the community to calculate an arbitrary loyalty rating of users so that the community can better gauge its influence over its members.









