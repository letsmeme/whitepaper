---
description: Module for others
---

# Other Systems

### Invitation System

Each LetsMeMe user has an initial set number of activation codes that they can share with their friends and family. After the initial invitation codes are used up, users will receive 1 new invitation code for every 10 fuels consumed.

### Anti-cheating System

LetsMeMe is developing an independent anti-cheating system that examines if accounts are real users rather than bots and the authenticity of the account’s followers.

Once a user is detected as a cheater, the user's account will be punished.

The specific punishment mechanism will be determined later.



## Tax & Fee System

| Content         | Fee Rate |
| --------------- | -------: |
| Marketplace Fee |       2% |
| Royalty Fee\*   |       4% |
| Mint            |       6% |



## Achievement Sytem

Will be introduced soon.

## Event System

Will be introduced soon.



