---
description: Bring the hype to your community
---

# 🏠 What is Trafinity

<figure><img src=".gitbook/assets/截屏2022-11-17 21.47.17.png" alt=""><figcaption><p>https://t.xyz</p></figcaption></figure>

TraFinity is a web3 traffic and community centric company that created Let's meme -- a community management and traffic-fi protocol tool designed to help thousands of future businesses, communities and brands better market their blockchain-based technology and their web3 vision.

## What is LetsMeMe?

LetsMeMe is the world's first trafficFi based community management platform. It is a social layer-2 network built upon traditional social media, i.e Twitter. LetsMeMe integrates web3 functionality with web2 social media platforms like Twitter and Youtube, to benefit and enhance crypto-native communities. Its existence will enable the creation of the largest web3 traffic network driven entirely by communities.



## Vision

* Empower businesses of all sizes to reach beyond their target audience and achieve their marketing goals.
* Create the farthest-reaching web3 traffic network



## Mission

* Allow all communities to manage and engage their members more easily.
* Destabilize the web2 traffic monopoly, allowing all organizations to advance their vision.



## Values

* Use advancing technology to drive marketing
* Holders themselves are a community’s builders



