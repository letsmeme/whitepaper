---
description: LetsMeMe Token
---

# $LMT

**LMT Tokenomics**

* **Platform governance rights**
* **Farm to gain financial income:** DAO Token's liquidity incentive
* **Exclusive NFT purchase rights**
* **Financing method of $DTS(DAO Tokens)**
